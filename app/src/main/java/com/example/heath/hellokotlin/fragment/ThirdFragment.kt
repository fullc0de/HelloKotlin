package com.example.heath.hellokotlin.fragment

import android.app.Fragment
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.heath.hellokotlin.R

/**
 * Created by heath on 2017. 1. 16..
 */
class ThirdFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("ThirdFragment", "onCreate")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d("ThirdFragment", "onCreateView")
        return inflater?.inflate(R.layout.fragment_third, container, false)
    }

    override fun onPause() {
        super.onPause()
        Log.d("ThirdFragment", "onPause")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("ThirdFragment", "onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("ThirdFragment", "onDestroy")
    }
}