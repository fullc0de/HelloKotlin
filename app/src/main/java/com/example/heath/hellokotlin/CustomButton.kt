package com.example.heath.hellokotlin

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView

/**
 * Created by heath on 2017. 1. 17..
 */
class CustomButton : TextView {
    constructor(context: Context) : super(context) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView()
    }

    fun initView() {
        
    }
}