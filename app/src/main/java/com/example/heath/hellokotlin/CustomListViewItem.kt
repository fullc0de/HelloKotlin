package com.example.heath.hellokotlin

import android.graphics.drawable.Drawable

/**
 * Created by heath on 2017. 1. 16..
 */
data class CustomListViewItem(
    var icon: Drawable? = null,
    var title: String = "Hello",
    var subTitle: String = "world"
)
