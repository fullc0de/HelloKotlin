package com.example.heath.hellokotlin

/**
 * Created by heath on 2017. 1. 11..
 */
object Model {
    data class Feed(
            val title: String,
            val items: List<Item>
    )

    data class Item(
            val title: String,
            val author: String,
            val media: Media
    )

    data class Media(
            val m: String
    )
}