package com.example.heath.hellokotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.activity_main.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    lateinit var service: Observable<Model.Feed>

    val disposables = CompositeDisposable()
    var photos = mutableListOf<Model.Item>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fetchPhoto()
        disposables.add(Observable.interval(2000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe() {
                    t -> updateDisplayedPhoto()
        })


        Observable.interval(2000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe() {
                    t -> updateDisplayedPhoto()
                }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    fun fetchPhoto() {
        service = FlickrService.publicPhoto()
        disposables.add(service.subscribe(
                { feed -> appendNewPhotos(feed.items) },
                { error -> Toast.makeText(this, error.message ?: "unknown error raised", Toast.LENGTH_SHORT).show() }
        ))
    }

    fun appendNewPhotos(photo: List<Model.Item>) {
        photos.addAll(photo)
        Log.d("bbb", "item count: " + photos.count() + "  thread: " + Thread.currentThread().name)
    }

    fun updateDisplayedPhoto() {
        if (photos.count() == 5) {
            fetchPhoto()
        }

        if (photos.count() > 0) {
            val urlstring = photos.removeAt(0).media.m.replace("_m", "")
            Log.d("ui", "url = " + urlstring)
            Glide.with(this).load(urlstring)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .dontAnimate()
                    .into(imageView)
        }
    }
}
