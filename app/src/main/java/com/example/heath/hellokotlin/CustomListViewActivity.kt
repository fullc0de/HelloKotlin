package com.example.heath.hellokotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_custom_list_view.*

class CustomListViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_list_view)

        val adapter = CustomListViewAdapter(this)
        list.adapter = adapter

        adapter.addItem("Welcome", "android world")
        adapter.addItem("Dragon", "Quest")
        adapter.addItem("Final", "Fantasy")

        adapter.setBtnClickListener { pos ->
            Toast.makeText(applicationContext, "$pos'th item is selected", Toast.LENGTH_SHORT).show()
        }

        list.setOnItemClickListener { adapterView, view, position, id ->
            val item = adapterView.getItemAtPosition(position) as CustomListViewItem
            Toast.makeText(applicationContext, "${item.title} ${item.subTitle}", Toast.LENGTH_SHORT).show()
        }
    }
}
