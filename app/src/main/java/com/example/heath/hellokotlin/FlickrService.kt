package com.example.heath.hellokotlin

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by heath on 2017. 1. 11..
 */
object FlickrService {

    fun publicPhoto(): Observable<Model.Feed> {

        val service = FlickrAPI.create()
        return service.getPublicPhoto()
                .subscribeOn(Schedulers.io())
                .doOnNext({ feed -> extraJob() })
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun extraJob() {
        Log.d("aaa", "extra job: " + Thread.currentThread().name)
    }
}