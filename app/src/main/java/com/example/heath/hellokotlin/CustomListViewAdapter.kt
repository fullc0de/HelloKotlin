package com.example.heath.hellokotlin

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import java.util.zip.Inflater

/**
 * Created by heath on 2017. 1. 16..
 */
class CustomListViewAdapter(context: Context) : BaseAdapter() {

    var itemList = mutableListOf<CustomListViewItem>()
    var btnClickLinstener: (Int) -> Unit = {}

    private var inflater: LayoutInflater

    init {
        inflater = LayoutInflater.from(context)
    }

    override fun getItem(position: Int): Any {
        return itemList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return itemList.count()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val itemView: View
        if (convertView == null) {
            itemView = inflater.inflate(R.layout.listview_item, parent, false)
        } else {
            itemView = convertView
        }

        val tv = itemView.findViewById(R.id.titleTextView) as TextView
        val stv = itemView.findViewById(R.id.subTitleTextView) as TextView
        val btn = itemView.findViewById(R.id.button) as Button
        val item = itemList[position]

        tv.text = item.title
        stv.text = item.subTitle

        btn.setOnClickListener { view -> btnClickLinstener(position) }

        return itemView
    }

    fun addItem(title: String, subTitle: String) {
        var item = CustomListViewItem(null, title, subTitle)
        itemList.add(item)
    }

    fun setBtnClickListener(listener: (pos: Int) -> Unit) {
        btnClickLinstener = listener
    }
}