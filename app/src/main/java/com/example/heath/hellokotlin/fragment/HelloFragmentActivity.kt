package com.example.heath.hellokotlin.fragment

import android.app.Fragment
import android.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.heath.hellokotlin.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_hello_fragment.*
import java.util.concurrent.TimeUnit

class HelloFragmentActivity : AppCompatActivity() {

    private lateinit var dynamicFragment: Fragment
    private lateinit var retainedFragment: RetainedFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello_fragment)

        dynamicFragment = SecondFragment()
        updateBottomFragment()

        button.setOnClickListener { v -> switchFragment() }

        val workFm = fragmentManager.findFragmentByTag("work")
        if (workFm == null) {
            retainedFragment = RetainedFragment()
            fragmentManager.beginTransaction()
                    .add(retainedFragment, "work")
                    .commit()
        } else {
            retainedFragment = workFm as RetainedFragment
            Log.d("activity", "RETAINED!")
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        Log.d("activity", "focus state = $hasFocus")
    }

    private fun switchFragment() {
        val newFragment: Fragment
        if (dynamicFragment is SecondFragment) {
            newFragment = ThirdFragment()
        } else {
            newFragment = SecondFragment()
        }
        dynamicFragment = newFragment
        updateBottomFragment()
    }

    private fun updateBottomFragment() {
        val ft = fragmentManager.beginTransaction()
        ft.setTransition(TRANSIT_FRAGMENT_FADE)
        ft.replace(R.id.fragment_bottom, dynamicFragment)
        ft.addToBackStack(null)
        ft.commit()
    }

    class RetainedFragment : Fragment() {

        val disposables = CompositeDisposable()

        override fun onCreate(savedInstanceState: Bundle?) {
            Log.d("retainedFragment", "onCreate")
            super.onCreate(savedInstanceState)
            retainInstance = true

            disposables.add(Observable.interval(1000, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        Log.d("retainedFragment", "Count = $it")
                    })
        }

        override fun onDestroy() {
            super.onDestroy()
            disposables.clear()
            Log.d("retainedFragment", "onDestroy")
        }
    }
}
