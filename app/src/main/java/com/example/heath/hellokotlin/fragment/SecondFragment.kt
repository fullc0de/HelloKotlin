package com.example.heath.hellokotlin.fragment

import android.app.Fragment
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.heath.hellokotlin.R

/**
 * Created by heath on 2017. 1. 16..
 */
class SecondFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("SecondFragment", "onCreate")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d("SecondFragment", "onCreateView")
        return inflater?.inflate(R.layout.fragment_second, container, false)
    }

    override fun onPause() {
        super.onPause()
        Log.d("SecondFragment", "onPause")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("SecondFragment", "onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("SecondFragment", "onDestroy")
    }
}