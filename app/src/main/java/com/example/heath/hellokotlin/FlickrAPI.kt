package com.example.heath.hellokotlin

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable

/**
 * Created by heath on 2017. 1. 11..
 */
interface FlickrAPI {
    @GET("services/feeds/photos_public.gne?format=json&nojsoncallback=1")
    fun getPublicPhoto(): Observable<Model.Feed>

    companion object {
        fun create() : FlickrAPI {
            val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.flickr.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            return retrofit.create(FlickrAPI::class.java)
        }
    }
}

