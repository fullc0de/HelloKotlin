package com.example.heath.hellokotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import kotlinx.android.synthetic.main.activity_list_view.*

class ListViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view)

        val values = (1..20).map { mapOf("item1" to "No. $it", "item2" to "Value = $it") }

        list.adapter = SimpleAdapter(this, values, android.R.layout.simple_list_item_2, arrayOf("item1", "item2"), intArrayOf(android.R.id.text1, android.R.id.text2))
        list.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            val title = view.findViewById(android.R.id.text1) as TextView
            val subTitle = view.findViewById(android.R.id.text2) as TextView

            Toast.makeText(applicationContext, "Position: $i, id: $l, Title: ${title.text}, Subtitle: ${subTitle.text}", Toast.LENGTH_SHORT).show()
        }
    }
}
